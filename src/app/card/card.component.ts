import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {

  @Input() cardDataList:any;
  @Output() updateEvent=new EventEmitter();
  public submit(){
    this.updateEvent.emit("data is updated");
  }
}
