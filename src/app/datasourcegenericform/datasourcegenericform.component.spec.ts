import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatasourcegenericformComponent } from './datasourcegenericform.component';

describe('DatasourcegenericformComponent', () => {
  let component: DatasourcegenericformComponent;
  let fixture: ComponentFixture<DatasourcegenericformComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DatasourcegenericformComponent]
    });
    fixture = TestBed.createComponent(DatasourcegenericformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
