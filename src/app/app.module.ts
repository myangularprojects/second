import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { ImageComponent } from './image/image.component';
import { CardComponent } from './card/card.component';
import { UserRegComponent } from './user-reg/user-reg.component';
import { FormsModule } from '@angular/forms';
import { DatasourcegenericformComponent } from './datasourcegenericform/datasourcegenericform.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    ImageComponent,
    CardComponent,
    UserRegComponent,
    DatasourcegenericformComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
