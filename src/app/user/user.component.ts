import { Component } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {

  public userList:any=[
    {
      name:"dharani",
      email:"dharandharani830@gmail.com",
      dapartment:"Mechatronics",
      experience:"two years",
      profilePicPath:"https://png.pngtree.com/png-vector/20190710/ourlarge/pngtree-business-user-profile-vector-png-image_1541960.jpg",
      qualification:"Bachelor Of Enginnering",
      address:[
        {
          city:"Rasipuram",
          state:"Tamilnadu",
          country:"India",
          pincode:"637408"
        }
      ]
    },
    {
      name:"bharathi",
      email:"dharandharani830@gmail.com",
      dapartment:"Mechatronics",
      experience:"two years",
      profilePicPath:"https://png.pngtree.com/png-vector/20190710/ourlarge/pngtree-business-user-profile-vector-png-image_1541960.jpg",
      qualification:"Bachelor Of science",
      address:[
        {
          city:"Rasipuram",
          state:"Tamilnadu",
          country:"India",
          pincode:"637408"
        }
      ]
    },
    {
      name:"raja",
      email:"dharandharani830@gmail.com",
      dapartment:"Mechatronics",
      experience:"two years",
      profilePicPath:"https://png.pngtree.com/png-vector/20190710/ourlarge/pngtree-business-user-profile-vector-png-image_1541960.jpg",
      qualification:"Bachelor Of Architecture",
      address:[
        {
          city:"Rasipuram",
          state:"Tamilnadu",
          country:"India",
          pincode:"637408"
        }
      ]
    },
  ]
  public receiveData(event:any){
      console.log(event);
  };
}
